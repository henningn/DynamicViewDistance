local ADDON_LOADED = "ADDON_LOADED"
local PLAYER_LEAVING_WORLD = "PLAYER_LEAVING_WORLD"
local PLAYER_ENTERING_WORLD = "PLAYER_ENTERING_WORLD"

local graphics = {
    graphicsViewDistance = { [0] = 1, [1] = 2, [2] = 3, [3] = 4, [4] = 5, [5] = 6, [6] = 7, [7] = 8, [8] = 9, [9] = 10 },
    textureFilteringMode = { [0] = 0, [1] = 1, [2] = 2, [3] = 3, [4] = 4, [5] = 5, [6] = 5, [7] = 5, [8] = 5, [9] = 5 },
    lodObjectCullSize = { [0] = 35, [1] = 30, [2] = 27, [3] = 22, [4] = 20, [5] = 19, [6] = 18, [7] = 17, [8] = 16, [9] = 14 } -- alternative for graphicsEnvironmentDetail
}

local run = true
local suppressWarning = false

local virtualFrame = CreateFrame("frame");
virtualFrame:RegisterEvent(ADDON_LOADED);
virtualFrame:SetScript("OnEvent", function(self, event)
    if event == ADDON_LOADED then
        self:SetScript("OnUpdate", updateCycle)
        self:UnregisterEvent(ADDON_LOADED)
        self:RegisterEvent(PLAYER_LEAVING_WORLD);
        self:RegisterEvent(PLAYER_ENTERING_WORLD);
    elseif event == PLAYER_LEAVING_WORLD then
        run = false
    elseif event == PLAYER_ENTERING_WORLD then
        run = true
        suppressWarning = false
    end
end)

local updateInterval = 1;
local timeSinceLastUpdate = 0
function updateCycle(_, elapsed)
    if run then
        timeSinceLastUpdate = timeSinceLastUpdate + elapsed;
        if (timeSinceLastUpdate > updateInterval) then
            updateTick()
            timeSinceLastUpdate = 0;
        end
    end
end

local lastFps = 0
local lastTargetFps = 0
local lastMaxFps = 0
function updateTick()
    local targetFps = tonumber(GetCVar('targetFPS'))
    local maxFps = tonumber(GetCVar('maxFPS'))

    if lastFps == 0 then
        lastFps = GetFramerate()
    end
    if targetFps ~= lastTargetFps or maxFps ~= lastMaxFps then
        lastTargetFps = targetFps
        lastMaxFps = maxFps
        suppressWarning = false
    end

    if targetFps <= 0 then
        showWarning("'Target FPS' is not set.")
    elseif maxFps <= 0 then
        showWarning("'Max Foreground FPS' is not set.")
    elseif targetFps >= maxFps then
        showWarning("'Target FPS' must be lower than 'Max Foreground FPS'.")
    else
        local fps = GetFramerate()
        local avgFps = 1 + (fps + lastFps) / 2
        lastFps = fps
        local viewDistance = tonumber(GetCVar('graphicsViewDistance'))

        if avgFps > maxFps and viewDistance < 10 then
            setGraphics(viewDistance + 1)
        elseif avgFps < targetFps and viewDistance > 1 then
            setGraphics(viewDistance - 1)
        end
    end
end

function showWarning(string)
    if not suppressWarning then
        dvdPrint("INACTIVE: " .. string)
        suppressWarning = true
    end
end

function setGraphics(value)
    SetCVar('graphicsViewDistance', graphics.graphicsViewDistance[value - 1])
    SetCVar('textureFilteringMode', graphics.textureFilteringMode[value - 1])
    SetCVar('lodObjectCullSize', graphics.lodObjectCullSize[value - 1])
end

SLASH_DYNAMICVIEWDISTANCE1 = "/dvd"
SLASH_DYNAMICVIEWDISTANCE2 = "/dynamicviewdistance"
SlashCmdList["DYNAMICVIEWDISTANCE"] = function(msg)
    slashCommand(msg)
end

function slashCommand(msg)
    suppressWarning = false
end

function dvdPrint(string)
    print("[DynamicViewDistance] " .. string)
end
